from selenium import webdriver
import pandas as pd

driver = webdriver.Chrome(r'C:\Users\imiha\AppData\Local\Programs\Python\Python38-32\Lib\site-packages'
                          r'\chromedriver\chromedriver.exe')
driver.get('https://www.bnr.ro/files/xml/nbrfxrates2019.htm')
tabel_head = driver.find_element_by_xpath('//*[@id="Data_table"]/table/thead')
# print(tabel_head.text)
tabel_body = driver.find_element_by_xpath('//*[@id="Data_table"]/table/tbody')
# print(tabel_body.text)

header = tabel_head.text.split('\n')
body = tabel_body.text.split('\n')

# TABEL GENERAT PE RANDURI
body_rows = []
counter = 0
for j in range(counter, len(body), len(header)):
    counter = j
    body_rows.append(body[counter:counter+len(header)])

df = pd.DataFrame(body_rows, columns=header)
df.to_excel("Tabel valuta BNR 2019_2.xls", index=0)
