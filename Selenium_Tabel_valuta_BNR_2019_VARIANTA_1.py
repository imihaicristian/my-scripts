from selenium import webdriver
import pandas as pd

driver = webdriver.Chrome(r'C:\Users\imiha\AppData\Local\Programs\Python\Python38-32\Lib\site-packages'
                          r'\chromedriver\chromedriver.exe')
driver.get('https://www.bnr.ro/files/xml/nbrfxrates2019.htm')
tabel_head = driver.find_element_by_xpath('//*[@id="Data_table"]/table/thead')
# print(tabel_head.text)
tabel_body = driver.find_element_by_xpath('//*[@id="Data_table"]/table/tbody')
# print(tabel_body.text)

header = tabel_head.text.split('\n')
body = tabel_body.text.split('\n')

# TABEL GENERAT PE COLOANE
body_columns = {key: [] for key in range(len(header))}
counter = 0
length_list = len(body_columns)
for j in body:
    body_columns[counter % length_list].append(j)
    counter += 1

df = pd.DataFrame(body_columns)
df.to_excel("Tabel valuta BNR 2019.xls", header=header, index=0)
