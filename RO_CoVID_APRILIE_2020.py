import requests
from bs4 import BeautifulSoup
import pandas as pd


def sheet_day(x):
    df = pd.DataFrame(all_table_data[x-3], columns=header)
    df.to_excel(writer, sheet_name=('{} aprilie 2020'.format(x)), index=0)
    return x


_7_aprilie = 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-6-aprilie-2020-ora-13-00-2/'
_17_aprilie = 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-16-aprilie-2020-ora-13-00-2/'

url_list = list()
lista_zile = list()
for zi in range(3, 31):
    url = 'https://www.mai.gov.ro/informare-covid-19-grupul-de-comunicare-strategica-' + str(zi) + '-aprilie-2020'\
          '-ora-13-00/'
    url_list.append(url)
    lista_zile.append(zi)
url_list.pop(4)
url_list.insert(4, _7_aprilie)
url_list.pop(14)
url_list.insert(14, _17_aprilie)

all_table_data = []
for page in url_list:
    req = requests.get(page)
    response = req.text
    link = BeautifulSoup(response, 'html.parser')
    title = link.find_all('div', attrs={'class': 'inside-article'})
    table = ''
    tbody = ''
    table_data = []
    for i in title:
        for table in i.find_all('table'):
            for tr in table.find_all('tr'):
                td_list = list()
                for td in tr.find_all('td'):
                    td_list.append(td.get_text().strip(' '))
                table_data.append(tuple(td_list))
                tbody += str(tr)
            header = table_data[0]
            table_data.pop(0)
            table_data.insert(-1, "")
            # print(table_data)
            break
    all_table_data.append(table_data)

    table = f'<body><table>{tbody}</table></body>'

    file = open("Situatie_aprilie_CoVID.html", 'a+', encoding="utf-8")
    file.writelines(table)
    file.close()

writer = pd.ExcelWriter("Situatie_aprilie_CoVID.xlsx", engine='xlsxwriter')
for sh_day in lista_zile:
    sheet_day(sh_day)
writer.save()
